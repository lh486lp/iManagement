/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50639
 Source Host           : localhost:3306
 Source Schema         : imanagement

 Target Server Type    : MySQL
 Target Server Version : 50639
 File Encoding         : 65001

 Date: 22/01/2018 09:43:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gms_user
-- ----------------------------
DROP TABLE IF EXISTS `gms_user`;
CREATE TABLE `gms_user` (
  `id` varchar(32) NOT NULL DEFAULT '0' COMMENT '主键，uuid',
  `role_id` varchar(8) NOT NULL DEFAULT '0' COMMENT '角色id',
  `user_code` varchar(50) NOT NULL COMMENT '用户登录帐号',
  `user_name` varchar(50) NOT NULL COMMENT '姓名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `login_count` int(11) DEFAULT '0' COMMENT '登录次数',
  `status` varchar(2) NOT NULL DEFAULT '0' COMMENT '状态 0 正常 1 失效 2删除',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `creat_user` varchar(50) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_ip` varchar(50) NOT NULL COMMENT '创建ip',
  `lastupdate_user` varchar(50) DEFAULT NULL COMMENT '最后更新人',
  `lastupdate_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `lastupdate_ip` varchar(50) DEFAULT NULL COMMENT '最后更新ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of gms_user
-- ----------------------------
BEGIN;
INSERT INTO `gms_user` VALUES ('e29cb37be0c9494f8ebc5d602dd2c7f0', '30000000', 'lh486lp', '卢惠', '/DZKuICn2TgMKWZ4n0Wy9A==', 2, '0', NULL, 'lh486lp', '2018-01-15 22:42:54', '0:0:0:0:0:0:0:1', 'lh486lp', '2018-01-16 20:24:10', '0:0:0:0:0:0:0:1');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(3) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `pid` int(3) NOT NULL COMMENT '上级菜单ID',
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `url` varchar(255) NOT NULL COMMENT '菜单地址',
  `visible` int(1) DEFAULT '1' COMMENT '是否可见：1、可见 2、不可见',
  `sort` int(3) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '', 1, 0);
INSERT INTO `sys_menu` VALUES (2, 0, '页面管理', '', 1, 2);
INSERT INTO `sys_menu` VALUES (3, 1, '菜单管理', '/menu', 1, 1);
INSERT INTO `sys_menu` VALUES (4, 3, '新增菜单', '/menu/addMenu', 1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
