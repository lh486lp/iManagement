package com.north.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.north.system"})
@EnableAutoConfiguration
public class AppStart {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AppStart.class, args);
	}
}
