package com.north.system.common.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.north.system.common.BaseController;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@RequestMapping("/")
	public String index(Map<String, Object> map) {
		
		map.put("username", "luhui");
		return "login/index";
	}
}
