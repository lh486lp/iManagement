package com.north.system.common.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.north.system.common.BaseController;
import com.north.system.common.entity.MenuView;
import com.north.system.common.entity.SysMenu;

@Controller
public class SystemController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);

	@RequestMapping("/")
	public String index(Integer pid, Map<String, Object> map) {
		logger.info(pid+"");
		Wrapper<SysMenu> wrapper = new EntityWrapper<SysMenu>();
		wrapper.eq("pid", "0");
		List<SysMenu> headerMenus = new SysMenu().selectList(wrapper);
		
		if(pid == null)
			pid = headerMenus.get(0).getId();
		
		wrapper = new EntityWrapper<SysMenu>();
		wrapper.eq("pid", pid);
		List<SysMenu> menus = new SysMenu().selectList(wrapper);
		
		// 使用google guava 包 对以获取的分类按照父类Id进行组装
		Multimap<Integer, SysMenu> menuMultimap = ArrayListMultimap.create();
		for (int i = 0; i < menus.size(); i++) {
			menuMultimap.put(menus.get(i).getPid(), menus.get(i));
		}
		List<MenuView> list = new ArrayList<MenuView>();

		list = subType(pid, menuMultimap, 1);// 递归调用 生成当前节点的子节点

		map.put("headerMenus", headerMenus);
		map.put("menuViews", list);
		return "common/index";
	}
	
	@RequestMapping("/main")
	public String main(Map<String, Object> map) {
		map.put("message", "hello world!");
		return "index";
	}
	
	@RequestMapping("/menu")
	public String menu(Map<String, Object> map) {
		List<SysMenu> menus = new SysMenu().selectAll();
		
		// 使用google guava 包 对以获取的分类按照父类Id进行组装
		Multimap<Integer, SysMenu> menuMultimap = ArrayListMultimap.create();
		for (int i = 0; i < menus.size(); i++) {
			menuMultimap.put(menus.get(i).getPid(), menus.get(i));
		}
		List<MenuView> list = new ArrayList<MenuView>();

		list = subType(0, menuMultimap, 0);// 递归调用 生成当前节点的子节点
		
		map.put("menuViews", list);
		map.put("message", "hello menus!");
		return "menu/index";
	}

	/**
	 * 递归处理多级分类问题
	 * 
	 * @param pid
	 *            父类Id
	 * @param maps
	 *            所有分类的按照父类ID组装后容器
	 * @param level
	 *            分类的级别 0：根
	 * @return 返回 pid 节点的子分类节点【可能是多个】
	 */
	private List<MenuView> subType(Integer pid, Multimap<Integer, SysMenu> maps, int level) {
		List<MenuView> list = new ArrayList<MenuView>();
		Collection<SysMenu> trList = maps.get(pid);
		for (Iterator<SysMenu> iterator = trList.iterator(); iterator.hasNext();) {
			SysMenu menuTemp = iterator.next();
			MenuView MenuView = new MenuView();
			MenuView.setMenu(menuTemp);
			MenuView.setLevel(level);
			list.add(MenuView);
			MenuView.setChildren(subType(menuTemp.getId(), maps, level + 1));
		}

		if (list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}
}
