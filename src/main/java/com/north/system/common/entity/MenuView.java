package com.north.system.common.entity;

import java.util.List;

public class MenuView {
    private SysMenu menu;//当前菜单  
    private List<MenuView> children ; //子菜单  
    private Integer level;
    
	public SysMenu getMenu() {
		return menu;
	}
	public void setMenu(SysMenu menu) {
		this.menu = menu;
	}
	public List<MenuView> getChildren() {
		return children;
	}
	public void setChildren(List<MenuView> children) {
		this.children = children;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	} 
}
