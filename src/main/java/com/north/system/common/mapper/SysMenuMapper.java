package com.north.system.common.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.north.system.common.entity.SysMenu;

public interface SysMenuMapper extends BaseMapper<SysMenu> {
	public List<SysMenu> getList(SysMenu sysMenu);
}