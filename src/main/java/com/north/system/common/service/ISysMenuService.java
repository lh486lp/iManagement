package com.north.system.common.service;

import com.baomidou.mybatisplus.service.IService;
import com.north.system.common.entity.SysMenu;

public interface ISysMenuService extends IService<SysMenu> {

}
