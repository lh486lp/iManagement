package com.north.system.common.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.north.system.common.entity.SysMenu;
import com.north.system.common.mapper.SysMenuMapper;
import com.north.system.common.service.ISysMenuService;

public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

}
