<#assign base=request.contextPath />
<#macro html page_title>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>管理后台</title>
<link rel="stylesheet" href="${base}/layui/css/layui.css">

<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="${base}/layui/layui.js"></script>
<script>
	//JavaScript代码区域
	layui.use('element', function() {
		var element = layui.element;
	});
</script>
</head>
<body class="layui-layout-body">
	<div class="layui-layout-admin">
  		<#include "./header.ftl">
  		<@header/>
  		
  		<#include "./left.ftl">
  		<@left/>
  		
		<#nested />

 		<#include "./footer.ftl">
  		<@footer/>
	</div>
</body>
</html>
</#macro>