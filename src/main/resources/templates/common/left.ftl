<#macro left>
	<div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
			<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
			<ul class="layui-nav layui-nav-tree" lay-filter="test">
				<#if menuViews?? && menuViews?size gt 0>
					<#list menuViews as view>
						<li class="layui-nav-item layui-nav-itemed">
							<a class="" href="javascript:;">${view.menu.name!''} </a>
							<#if view.children?? && view.children?size gt 0> 
								<#list view.children as item>
									<dl class="layui-nav-child">
										<dd><a href="${item.menu.url}" target="mainFrame">${item.menu.name} </a></dd>
									</dl>
								</#list>
							</#if>
						</li>
					</#list>
				</#if>
			</ul>
		</div>
	</div>
</#macro>