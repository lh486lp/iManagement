<div class="layui-body">
	<div style="padding: 15px;">message:${message}</div>
	<ul>
		<#list menuViews as view>
			<li class="layui-nav-item layui-nav-itemed">
				
				<div style="padding: 15px;">name:${view.menu.name!''}</div>
				<#if view.children?? && view.children?size gt 0> 
					<#list view.children as item>
						<dl class="layui-nav-child">
							<dd><div style="padding: 15px;">url:${item.menu.url}, name:${view.menu.name!''}</div></dd>
						</dl>
					</#list>
				</#if>
			</li>
		</#list>
	</ul>
</div>
